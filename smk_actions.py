import logging
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import time
import sys
import csv
import pandas
import collections

logger = logging.getLogger('smk_actions')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

def smk_login(config, driver):
    try:
        logger.info("click: zaloguj")
        login_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.login_btn)))
        login_btn.click()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    # zalogowanie
    try:
        logger.info("Log in using config credentials")
        login_field = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.login_field)))
        password_field = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.password_field)))
        login_btn2 = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.login_btn2)))
        login_field.send_keys(config.login)
        password_field.send_keys(config.password)
        login_btn2.click()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_select_role(config, driver):
    try:
        logger.info("click: Rola")
        role_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.role_btn)))
        ActionChains(driver).move_to_element(role_btn).click(role_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    try:
        logger.info("click: eks")
        eks_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.eks_btn)))
        ActionChains(driver).move_to_element(eks_btn).click(eks_btn).perform()
    except TimeoutException:
        sys.exit("Error: Loading took too much time! Exiting!")

    # klikniecie strzałki eks
    try:
        logger.info("click: strzałka w eks")
        eks_arrow = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.eks_arrow_btn)))
        driver.execute_script('arguments[0].scrollIntoView({block: "center", inline: "center"})', eks_arrow)
        time.sleep(1)
        ActionChains(driver).move_to_element(eks_arrow).click(eks_arrow).perform()
    except TimeoutException:
        sys.exit("Error: Loading took too much time! Exiting!")

    # kliknięcie edycji w eks
    try:
        logger.info("click: edycja w eks")
        edit_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.eks_edit_btn)))
        ActionChains(driver).move_to_element(edit_btn).click(edit_btn).perform()
    except TimeoutException:
        sys.exit("Error: Loading took too much time! Exiting!")

def smk_iwzpm(config, driver):
    try:
        logger.info("click: iwzpm")
        iwzpm_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.iwzpm_btn)))
        driver.execute_script('arguments[0].scrollIntoView({block: "center", inline: "center"})', iwzpm_btn)
        time.sleep(1)
        ActionChains(driver).move_to_element(iwzpm_btn).click(iwzpm_btn).perform() 
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_expand_iwzpm(config, driver):
    try:
        logger.info("expand: iwzpm")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.iwzpm_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_expand_zwzorzc(config, driver):
    try:
        logger.info("expand: znieczulenia w zależności od rodzaju zabiegów chirurgicznych")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.zwzorzc_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_fill_zwl(config, driver, filename):
    try:
        logger.info("expand: znieczulenie w laryngologii")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.zwl_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.zwl_add_btn)

def smk_fill_zwn(config, driver, filename):
    try:
        logger.info("expand: znieczulenie w neurochirurgii")
        time.sleep(1)
        expand_btn3 = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.zwn_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn3).click(expand_btn3).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.zwn_add_btn)

def smk_expand_rz(config, driver):
    try:
        logger.info("expand: rodzaj zneczulenia")
        time.sleep(1)
        expand_btn2 = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.rz_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn2).click(expand_btn2).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_fill_zo(config, driver, filename):
    try:
        logger.info("expand: znieczulenie ogólne")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH,config.xpaths.zo_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.zo_add_btn)

def smk_fill_olpa(config, driver, filename):
    try:
        logger.info("expand: znieczulenie ogólne")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH,config.xpaths.olpa_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.olpa_add_btn)

def smk_expand_cew(config, driver):
    try:
        logger.info("expand: Cewnikowanie")
        time.sleep(1)
        expand_btn2 = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, config.xpaths.cew_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn2).click(expand_btn2).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

def smk_fill_ct(config, driver, filename):
    try:
        logger.info("expand:  Tętnicy")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH,config.xpaths.ct_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.ct_add_btn)

def smk_fill_czc(config, driver, filename):
    try:
        logger.info("expand: Żyły centralnej")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH,config.xpaths.czc_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.czc_add_btn)

def smk_fill_czcusg(config, driver, filename):
    try:
        logger.info("expand:  Cewnikowanie żyły centralnej przy użyciu USG")
        time.sleep(1)
        expand_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH,config.xpaths.czcusg_expand_btn)))
        ActionChains(driver).move_to_element(expand_btn).click(expand_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    add_rows_to_smk(filename,driver,config,config.xpaths.czcusg_add_btn)


def add_rows_to_smk(filename,driver,config,add_btn_xpath):
    df = pandas.read_excel(filename)
    row_count = len(df.index)
    done_count = 0
    logger.info("Rows found in file: {}".format(row_count))
    for _, row in df.iterrows():
        smk_fill_row(config,driver,row,add_btn_xpath)
        done_count += 1
        logger.info('Rows done {} / {}'.format(done_count,row_count))

def smk_fill_row(config,driver,row,btn_xpath):
    try:
        logger.info("Begin adding row...")
        add_btn = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.XPATH, btn_xpath)))
        ActionChains(driver).move_to_element(add_btn).click(add_btn).perform()
    except TimeoutException:
        logger.error("Loading took too much time! Exiting at {}!".format(__name__))
        sys.exit()

    if(config.debug):
        row = WebDriverWait(driver, config.delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'tr[__gwt_row="0"]'))) 
        inputs = row.find_elements_by_css_selector('input')
        selects = row.find_elements_by_css_selector('select')

        logger.debug("selects found: {}".format(len(selects)))
        for smk_select in selects:
            logger.debug(smk_select.tag_name)
            logger.debug(smk_select.text)

        logger.debug("inputs found: {}".format(len(inputs)))
        for smk_input in inputs:
            logger.debug(smk_input.tag_name)
            logger.debug(smk_input.text)
        sys.exit()

    try:
        smk_row_input_enter(driver, str(row[config.excel.date].isoformat()), config.selectors.smk_date_cell)
    except Exception as e:
        logger.error('Enter date failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_date_cell))
        logger.debug('value/index: {}'.format(str(row[config.excel.date].isoformat())))
        sys.exit()

    try:
        smk_row_select_set_by_value(driver, str(row[config.excel.year]), config.selectors.smk_year_cell)    
    except Exception as e:
        logger.error('Set year failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_year_cell))
        logger.debug('value/index: {}'.format(row[config.excel.year]))
        sys.exit()

    try:
        smk_row_select_set_by_index(driver, str(row[config.excel.code_index]), config.selectors.smk_code_asist_cell)      
    except Exception as e:
        logger.error('Set code failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_code_asist_cell))
        logger.debug('value/index: {}'.format(row[config.excel.code_index]))
        sys.exit()

    try:
        smk_row_input_enter(driver, row[config.excel.executing_person], config.selectors.smk_executing_person_cell)  
    except Exception as e:
        logger.error('Enter executing person failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_executing_person_cell))
        logger.debug('value/index: {}'.format(row[config.excel.executing_person]))
        sys.exit()

    try:
        smk_row_select_set_by_index(driver, str(row[config.excel.place_index]), config.selectors.smk_place_cell)      
    except Exception as e:
        logger.error('Set place failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_place_cell))
        logger.debug('value/index: {}'.format(row[config.excel.place_index]))
        sys.exit()

    try:
        smk_row_select_set_by_index(driver, str(row[config.excel.intership_index]), config.selectors.smk_intership_cell)   
    except Exception as e:
        logger.error('Set intership failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_intership_cell))
        logger.debug('value/index: {}'.format(row[config.excel.intership_index]))
        sys.exit()

    try:
        smk_row_input_enter(driver, row[config.excel.initials], config.selectors.smk_initials_cell)  
    except Exception as e:
        logger.error('Enter initials failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_initials_cell))
        logger.debug('value/index: {}'.format(row[config.excel.initials]))
        sys.exit()

    try:
        smk_row_select_set_by_value(driver, row[config.excel.sex], config.selectors.smk_sex_cell)
    except Exception as e:
        logger.error('Set sex failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_sex_cell))
        logger.debug('value/index: {}'.format(row[config.excel.sex]))
        sys.exit()

    try:
        smk_row_input_enter(driver, row[config.excel.procedure_group], config.selectors.smk_procedure_group_cell) 
    except Exception as e:
        logger.error('Enter procedure group failed! Exiting!')
        logger.debug(e)
        logger.debug('Selector: {}'.format(config.selectors.smk_procedure_group_cell))
        logger.debug('value/index: {}'.format(row[config.excel.procedure_group]))
        sys.exit()

    logger.info('...row added')

def smk_row_input_enter(driver, value, selector):
    smk_input = driver.find_element_by_css_selector(selector) 
    #ActionChains(driver).move_to_element(smk_input).click().perform()
    smk_input.send_keys(value)
    #time.sleep(1)

def smk_row_select_set_by_value(driver, value, selector):
    smk_select = driver.find_element_by_css_selector(selector)
    #ActionChains(driver).move_to_element(smk_select).click().perform()
    Select(smk_select).select_by_value(value)
    #time.sleep(1)

def smk_row_select_set_by_index(driver, index, selector):
    smk_select = driver.find_element_by_css_selector(selector)
    #ActionChains(driver).move_to_element(smk_select).click().perform()
    Select(smk_select).select_by_index(index)
    #time.sleep(1)