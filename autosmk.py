from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select

import argparse
import os
import logging
from pathlib import Path
from smk_config import Config
from smk_actions import *

def smk_common(config, browser):
    driver = {}
    if browser == 'chrome':
        driver = webdriver.Chrome()
    elif browser == 'firefox':
        driver = webdriver.Firefox()
    else:
        logger.error('No browser or invalid browser argument provided. Provided was {}... Expected chrome or firefox'.format(args.browser))
        sys.exit()
    driver.maximize_window()   
    driver.get(config.smk_url)
    smk_login(config, driver)
    smk_select_role(config, driver)
    smk_iwzpm(config, driver)
    smk_expand_iwzpm(config, driver)
    return driver

logger = logging.getLogger('autosmk')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

parser = argparse.ArgumentParser(
    description='autosmk is a script to handle smk tasks at the speed of light',
    epilog="In case of issues please contact me via GitLab or send an email here: incoming+jarogn-autosmk-24308313-issue-@incoming.gitlab.com")
parser.add_argument('browser',
                    help='browser to be used as marionette. Valid values are: chrome,firefox')
parser.add_argument('smk_action_path', 
                    help='name fo smk_action task. Valid values are: zwl, zwn, zo, ct, czc, czcusg, olpa')
parser.add_argument('file',
                    help='file name of excel fille template filled with data to be imported to smk')

args = parser.parse_args()

#check if file exist
if not os.path.exists(Path(args.file)):
    logger.error('File: {} does not exist!'.format(args.file))
    sys.exit()

config = Config().get_configs()

#check if smk login and password is saved in the config
if(not config.login and not config.password):
    logger.error('SMK login and password not found in confing.json file! Open config.json in notepad and add login and password in correct place.')
    sys.exit()
elif(not config.login):
    logger.error('SMK login not found in confing.json file! Open config.json in notepad and add login in correct place.')
    sys.exit()
elif(not config.password):
    logger.error('SMK password not found in confing.json file! Open config.json in notepad and add password in correct place.')
    sys.exit()


if(args.smk_action_path == "zwl"):
    logger.info('Starting task: znieczulenia w laryngologii')
    driver = smk_common(config,args.browser)
    smk_expand_zwzorzc(config, driver)
    smk_fill_zwl(config, driver, args.file)
elif(args.smk_action_path == "zwn"):
    logger.info('Starting task: znieczulenia w neurochirurgi')
    driver = smk_common(config,args.browser)
    smk_expand_zwzorzc(config, driver)
    smk_fill_zwn(config, driver, args.file)
elif(args.smk_action_path == "zo"):
    logger.info('Starting task: znieczulenia ogólne')
    driver = smk_common(config,args.browser)
    smk_expand_rz(config, driver)
    smk_fill_zo(config, driver, args.file)
elif(args.smk_action_path == "ct"):
    logger.info('Starting task: Cewnikowanie - Tętnicy')
    driver = smk_common(config,args.browser)
    smk_expand_cew(config, driver)
    smk_fill_ct(config, driver, args.file)
elif(args.smk_action_path == "czc"):
    logger.info('Starting task: Cewnikowanie - Żyły centralnej')
    driver = smk_common(config,args.browser)
    smk_expand_cew(config, driver)
    smk_fill_czc(config, driver, args.file)
elif(args.smk_action_path == "czcusg"):
    logger.info('Starting task:  Cewnikowanie żyły centralnej przy użyciu USG')
    driver = smk_common(config,args.browser)
    smk_expand_cew(config, driver)
    smk_fill_czcusg(config, driver, args.file)
elif(args.smk_action_path == "olpa"):
    logger.info('Starting task: Ogólna liczba procedur anestezjologicznych (chorzy > 15 r.ż.)')
    driver = smk_common(config,args.browser)
    smk_expand_rz(config, driver)
    smk_fill_olpa(config, driver, args.file)
else:
    logger.error('Invalid smk_action provided')
    sys.exit()

