import logging
from python_json_config import ConfigBuilder

class Config():
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.config = self.get_configs()

    def get_configs(self):
        self.logger.debug('Loading configuration...')

        builder = ConfigBuilder()
        config = builder.parse_config({})
        config_filename = 'config.json'

        try:
            self.logger.debug('Config file path: {}'.format(config_filename))
            config = builder.parse_config(config_filename)
        except Exception:
            self.logger.critical('Could not load config file')
            raise ValueError('Could not load any initial config')
        return config