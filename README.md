# AutoSMK

EN: Python script to quickly import Excel spreadsheet into SMK (System Monitorowania Kształcenia)

PL: Skrypt Pythona do szybkiego importowania arkusza Excela do SMK (System Monitorowania Kształcenia)


EN: Report a problem by sending an email to: incoming+jarogn-autosmk-24308313-issue-@incoming.gitlab.com

PL: Zgłoś problem wysyłając email na adres: incoming+jarogn-autosmk-24308313-issue-@incoming.gitlab.com